# set base image (host OS)
FROM python:3
# FROM frankwolf/rpi-python3

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY .env .
ADD cotv_bot ./cotv_bot

# command to run on container start
CMD [ "python", "-m", "cotv_bot" ]
