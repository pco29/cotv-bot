"""
__version__.py
~~~~~~~~~~~~~~

Information about the current version of the py-package-template package.
"""

__title__ = 'cotv_bot'
__description__ = 'scrapper for the website https://reality.idnes.cz'
__version__ = '0.2.0'
__author__ = 'Felipe Bombardelli'
__author_email__ = 'bombardelli@protonmail.com'
__license__ = 'Apache 2.0'
__url__ = ''
