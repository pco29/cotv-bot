"""
py_pkg.entry_points.py
~~~~~~~~~~~~~~~~~~~~~~

This module contains the entry-point functions for the py_pkg module,
that are referenced in setup.py.
"""

# =============================================================================
#  Header
# =============================================================================

import requests
import os
import time
import scrapetube
#import plyvel
import leveldb
from datetime import datetime
import telegram
from decouple import config
from threading import Thread
import queue
from random import randrange
import logging
# sudo apt-get install python3-dev



g_bot_token = config('TOKEN')
g_chat_id = int(config('CHAT_ID'))
g_db = leveldb.LevelDB('./database', create_if_missing=True)

# logger=logging.getLogger()
# logger.setLevel(logging.DEBUG)

print("TOKEN:", g_bot_token)
print("CHAT_ID:", g_chat_id)

# =============================================================================
#  BotSender
# =============================================================================

class BotSender:
    def __init__(self, bot_token: str):
        self.bot = telegram.Bot(bot_token)
        self.queue = queue.Queue()
        self.thread = Thread(target=self.ctrl_run)

    def start(self):
        self.thread.start();

    def send_new_video(self, video_id: str, video_title: str):
        self.queue.put({'title': video_title, 'youtube_id': video_id})

    def ctrl_run(self):
        # logger.info('Criado o bot enviador do telegram')
        while(True):
            msg = self.queue.get()
            self.ctrl_send_video(msg['youtube_id'], msg['title'])
            minutes_wait = randrange(10) + 1
            time.sleep(minutes_wait*30)

    def ctrl_send_video(self, video_id: str, video_title: str, channel_name):
        # envia o link para o canal do telegram
        youtube_url = "https://www.youtube.com/watch?v={} | {} ".format(video_id, channel_name)
        # print("send["+video_id+"]: "+video_title)
        try:
            self.bot.send_message(text=youtube_url, chat_id=g_chat_id, parse_mode='HTML')
            g_db.Put( video_id.encode(), str(datetime.now()).encode() )
        except Exception as e:
            print("Error: ", e)


    def ctrl_send_video_with_audio(self, video_id: str, video_title: str, channel_name):
        # envia o link para o canal do telegram
        youtube_url = "https://www.youtube.com/watch?v={} | {}".format(video_id, channel_name)
        # print("send["+video_id+"]: "+video_title)
        try:
            self.bot.send_message(text=youtube_url, chat_id=g_chat_id, parse_mode='HTML')
            g_db.Put( video_id.encode(), str(datetime.now()).encode() )
        except Exception as e:
            print("Error: ", e)
        # autor
        autor = channel_name
        title_splitted = video_title.split("|")
        if len(title_splitted) == 2:
             video_title = title_splitted[0]
             autor = title_splitted[1]

        # envia o audio para o canal do telegram
        try:
            audio_fsurl = "audios/audio.mp3"
            if os.path.exists(audio_fsurl):
                os.remove(audio_fsurl)

            # 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'
            cmd = "yt-dlp -x --audio-format mp3 '{}' -o '{}'".format(youtube_url, audio_fsurl)
            os.system(cmd)

            self.bot.send_audio(chat_id=g_chat_id, title=video_title[0:48], caption=video_title, performer=autor, audio=open(audio_fsurl, 'rb'))
        except:
            print("error ao enviar o audio")

g_bot = BotSender(g_bot_token);

# =============================================================================
#  Youtube Scrapper
# =============================================================================
# type: videos, shorts, streams
def scrapper_new_videos(channel_name: str, channel_id: str, length, video_type):
    # get the list
    videos = scrapetube.get_channel(channel_id, sort_by="newest", limit=length, content_type=video_type)
    revert_videos = []
    for video in videos:
        revert_videos.append({"id": video["videoId"], "title": video["title"]["runs"][0]["text"]})

    # revert the list
    if len(revert_videos) < length:
        length = len(revert_videos)

    # Check if already sent or put in the queue to send
    list_to_send = []
    for i in range(length-1, -1, -1):
        video = revert_videos[i]
        video_id = video["id"]
        video_title = video["title"]
        try:
            isNotSent = False
            val = g_db.Get(video_id.encode())
        except KeyError:
            isNotSent = True

        if isNotSent:
            list_to_send += [{"channel": channel_name, "type": video_type, "id": video_id, "title": video_title}]

    print(channel_name, len(list_to_send))
    return list_to_send

def send_list(videos: []):
    for video in videos:
        print(video)
        if video["type"] == "videos":
            g_bot.ctrl_send_video_with_audio(video["id"], video["title"], video["channel"])
        else:
            g_bot.ctrl_send_video(video["id"], video["title"], video["channel"])
        time.sleep(5*60)

    if len(videos) == 0:
        time.sleep(5*60)


# =============================================================================
#  Main
# =============================================================================

# https://www.youtube.com/c/CausaOperariaTV
# youtube_channel_id = "UCEaXaeR0DL62WwRlR2EMxEQ"

cotv = "UC7seCBK7L7QPd1mDOQVse8g"
comando_de_greve = "UCCAtzoC9ZcNmJ_Rh0rsJx5w"
reuniao_de_pauta = "UCLJs1GZuTqwWZWKQuSxdYKw"
rui_costa = "UC_rCOOXBLtjjXPSlgBiAK7w"
radio_causa = "UCd9jGfdOhFqoHl8Fd8OJDAg"
dco = "UCvhnOzbSDblzftMPLe8D4-A"
while (True):
    videos = []
    videos += scrapper_new_videos("DCO", dco, 2, "videos")
    videos += scrapper_new_videos("DCO", dco, 2, "streams")
    videos += scrapper_new_videos("COTV", cotv, 5, "videos")
    videos += scrapper_new_videos("COTV", cotv, 5, "streams")
    videos += scrapper_new_videos("Comando de Greve", comando_de_greve, 2, "videos")
    videos += scrapper_new_videos("Comando de Greve", comando_de_greve, 2, "streams")
    videos += scrapper_new_videos("Rui Costa Pimenta", rui_costa, 1, "videos")
    videos += scrapper_new_videos("Rui Costa Pimenta", rui_costa, 1, "streams")
    videos += scrapper_new_videos("Reunião de Pauta", reuniao_de_pauta, 2, "videos")
    videos += scrapper_new_videos("Reunião de Pauta", reuniao_de_pauta, 2, "streams")
    videos += scrapper_new_videos("Rádio Causa Operária", radio_causa, 2, "videos")
    videos += scrapper_new_videos("Rádio Causa Operária", radio_causa, 2, "streams")
    send_list(videos)

