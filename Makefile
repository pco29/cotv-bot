docker:
	docker build -t cotv_bot .

run: docker
	docker run -d --name cotv_bot -v cotv_bot_db:/code/database cotv_bot